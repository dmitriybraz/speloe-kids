$(document).on('change', '.zangdar__step__active input', function() {
    $('.zangdar__step__active .btn_calc_next_wrap').addClass('active');
    $('.zangdar__step__active .btn_calc_next_wrap button').removeClass('disabled');
});

function formatNumber(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1 ')
};

//Определение обьекта с данными для калькулятора
let obj = new Object(),
    isHoliday,
    pPrice,
    age,
    chosenGift,
    checkOut,
    Rate,
    dopServ,
    qC,
    qA;

//Проверка является ли введеная дата выходным
function checkHoliday(){
    let date = new Date($('#datepicker').datepicker( "getDate" ));
    obj.isHoliday = date.getDay();
    // if 6 or 0 then day is holiday
};

$('section.age .btn_calc_next').on('click', function(){

    //Добавляем акции в зависимости от возраста детей
    let age = +$('#input_age').val();
    if( age >= 3 && age <= 6){
        $('section.gift .form-check').addClass('active');
    }else{
        $('section.gift .akciya-1').addClass('active');
    }
});

$('section.gift .btn_calc_back').on('click', function(){

    //Убираем акции при переходе на предыдущий пункт
    let age = +$('#input_age').val();
    if( age >= 3 && age <= 6){
        $('section.gift .form-check').removeClass('active');
    }else{
        $('section.gift .form-check.akciya-1').removeClass('active');
    }
});

$('section.dopservice_section .btn_calc_next').on('click', function(){
    checkHoliday();
    let values = [];
    $('input[type="checkbox"]:checked').each(function() {
        values.push(+$(this).val());
    });
    let sum = 0;
    for (let key in values) {
      sum += values[key];
    }
    console.log(sum);
    obj.age = +$('#input_age').val();
    obj.chosenGift = +$('section.gift input:checked').val();
    obj.qC = +$('#inp_num_child').val();
    obj.qA = +$('#inp_num_adult').val();
    obj.Rate = +$('section.rates input:checked').val();
    obj.dopServ = sum;

            //Определение цены за человека
            if (obj.isHoliday == 6 || obj.isHoliday == 0){
                if(obj.chosenGift == 2){
                    obj.pPrice = 175;
                }else if(obj.chosenGift == 1){
                    obj.pPrice = 350;
                }else{
                    obj.pPrice = 350;
                };
            }
            else{
                if(obj.chosenGift == 2){
                    obj.pPrice = 150;
                }else if(obj.chosenGift == 1){
                    obj.pPrice = 300;
                }else{
                    obj.pPrice = 300;
                };
            };

    if (obj.age >= 3 && obj.age <= 6){
        //Определение стоимости без учета тарифа
        if(obj.chosenGift == 2){
            if (obj.qC > 10){
                obj.checkOut = (obj.pPrice * (obj.qC + obj.qA)) - obj.pPrice + 1600;
            }else{
                obj.checkOut = (obj.pPrice * (obj.qC + obj.qA)) - obj.pPrice;
            }
        }else if(obj.chosenGift == 1){
            if (obj.qC > 10){
                obj.checkOut = (obj.pPrice * (obj.qC + obj.qA)) - (obj.pPrice*3) + 1600;
            }else{
                obj.checkOut = (obj.pPrice * (obj.qC + obj.qA)) - obj.pPrice;
            }
        }else{
            obj.checkOut = (obj.pPrice * (obj.qC + obj.qA));
        };
    }
    else if(obj.age >= 7){
        if(obj.chosenGift == 1){
            if (obj.qC > 13){
                obj.checkOut = (obj.pPrice * (obj.qC + obj.qA)) - (obj.pPrice*3) + 1600;
            }else{
                obj.checkOut = (obj.pPrice * (obj.qC + obj.qA)) - (obj.pPrice*3);
            }
        }else{
            obj.checkOut = (obj.pPrice * (obj.qC + obj.qA));
        };
    }
    else{
        obj.checkOut = (obj.pPrice * (obj.qC + obj.qA));
    };

    let finalPrice = formatNumber(obj.checkOut + obj.Rate + obj.dopServ);

    $('.calc_s6_sum').text(finalPrice);
    console.table(obj)
});

$.datepicker.regional['ru'] = {
	closeText: 'Закрыть',
	prevText: 'Предыдущий',
	nextText: 'Следующий',
	currentText: 'Сегодня',
	monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь','Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
	monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн','Июл','Авг','Сен','Окт','Ноя','Дек'],
	dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
	dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
	dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
	weekHeader: 'Не',
	dateFormat: 'dd.mm.yy',
	firstDay: 1,
	isRTL: false,
	showMonthAfterYear: false,
	yearSuffix: ''
};
$.datepicker.setDefaults($.datepicker.regional['ru']);

$(function(){
	$("#datepicker").datepicker({
	    minDate: 0
	});
});

function calcSlider222(){
    $(".step5_slider").slick({

  // normal options...
  infinite: false,
  slidesToShow: 4,
    slidesToScroll: 1,
    arrows: true,
    nextArrow:
    	'<button type="button" class="slick-next"><i class="fa fa-chevron-right" aria-hidden="true"></i></button>',
    prevArrow:
    	'<button type="button" class="slick-prev"><i class="fa fa-chevron-left" aria-hidden="true"></i></button>',
    	
      // the magic
    responsive: [{

      breakpoint: 1199,
      settings: {
        slidesToShow: 3,
      }

    },{

      breakpoint: 768,
      settings: {
        slidesToShow: 2,
      }

    }, {

      breakpoint: 576,
      settings: {
        slidesToShow: 1,
      }

    }, {

      breakpoint: 300,
      settings: "unslick" // destroys slick

    }]
});
	$(".step5_slider").on('wheel', (function(e) {
      e.preventDefault();
    
      if (e.originalEvent.deltaY > 0) {
        $(this).slick('slickNext');
      } else {
        $(this).slick('slickPrev');
      }
    }));
}


$(document).on('click', 'section.quantity .btn_calc_next', function(){
    setTimeout(calcSlider222(), 1);
});
