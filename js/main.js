"use strict";

$(document).ready(function () {
	// мобильное меню

	$(".menu_btn").on("click", function (e) {
		e.preventDefault();
		$(".m_menu").toggleClass("menu_active");
		$(".menu_btn").toggleClass("menu_btn_active");
		$(".m_menu_back").toggleClass("m_menu_back_active");
		$(".menu_btn i").toggleClass("m_menu_btn_show");
	});

	$(".m_menu_back").on("click", function () {
		$(".m_menu").removeClass("menu_active");
		$(".menu_btn").removeClass("menu_btn_active");
		$(".m_menu_back").removeClass("m_menu_back_active");
		$(".menu_btn i").removeClass("m_menu_btn_show");
	});

	$(".mob_nav a").on("click", function () {
		$(".m_menu").removeClass("menu_active");
		$(".menu_btn").removeClass("menu_btn_active");
		$(".m_menu_back").removeClass("m_menu_back_active");
		$(".menu_btn i").removeClass("m_menu_btn_show");
	});

	// кнопка прокрутки ко второму блоку

	$(".b1_btn_dwn").on("click", function (e) {
		e.preventDefault();
		$("html, body").animate(
			{
				scrollTop: $(".block_2").offset().top,
			},
			"300"
		);
	});

	// фильтр и слайдер аниматоры

	$(function () {
		$("#b3_carousel").bind("slide.bs.carousel", function (e) {
			let image = $(e.relatedTarget).find("img[data-src]");
			image.attr("src", image.data("src"));
			image.removeAttr("data-src");
		});
	});

	function showFirstAnim() {
		$(".carousel-item:first").addClass("active");
		let img = $(".carousel-item.active img");
		let ds = img.attr("data-src");
		if (ds != undefined) {
			img.attr("src", img.data("src"));
			img.removeAttr("data-src");
		}
	}
	function showAllAnim() {
		$(".carousel-indicators li").remove();
		$(".carousel-item").removeClass("active");
		$(".b3_slide").addClass("carousel-item");
		$(".b3_slide").removeClass("d-none");
		showFirstAnim();
		let dots = $(".carousel-item").length;
		let n = 0;
		for (dots; dots > 0; dots--) {
			$(".carousel-indicators").append(
				'<li data-target="#b3_carousel" data-slide-to="' + n++ + '"></li>'
			);
		}
		$(".carousel-indicators li:first-of-type").addClass("active");
	}

	function filterAnim() {
		let items = $(".b3_slide");
		$(".carousel-indicators li").remove();
		$(".carousel-item").removeClass("active");
		let age = $("#b3_age").val();
		let gen = $("#b3_gender").val();
		let n = 0;
		for (let i = 0; i < items.length; i++) {
			if ($(items[i]).hasClass(gen)) {
				if ($(items[i]).hasClass(age)) {
					$(items[i]).addClass("carousel-item");
					$(items[i]).removeClass("d-none");
					$(".carousel-indicators").append(
						'<li data-target="#b3_carousel" data-slide-to="' + n++ + '"></li>'
					);
				} else {
					$(items[i]).removeClass("carousel-item");
					$(items[i]).addClass("d-none");
				}
			} else {
				$(items[i]).removeClass("carousel-item");
				$(items[i]).addClass("d-none");
			}
		}
		showFirstAnim();
		$(".carousel-indicators li:first-of-type").addClass("active");
	}

	filterAnim();

	$("#b3_gender").on("change", function () {
		filterAnim();
	});

	$("#b3_age").on("change", function () {
		filterAnim();
	});

	$(".b3_btn_all").on("click", function () {
		showAllAnim();
	});

	$("#b3_carousel").on("wheel", function (e) {
		e.preventDefault();

		if (e.originalEvent.deltaY > 0) {
			$(this).carousel("next");
		} else {
			$(this).carousel("prev");
		}
	});

	// коллпас тарифы

	$(".b7_frame .collapse").on("show.bs.collapse", function (e) {
		let a = $(e.target);
		let b = $(a).parent();
		let c = $(b).children(".b7_link");
		c.html("Свернуть...");
	});

	$(".b7_frame .collapse").on("hide.bs.collapse", function (e) {
		let a = $(e.target);
		let b = $(a).parent();
		let c = $(b).children(".b7_link");
		c.html("Развернуть...");
	});

	$("#b7_other").on("show.bs.collapse", function (e) {
		let a = e.target.id;
		let b = this.id;
		if (a == b) {
			$(".other_progs").html("Свернуть");
			$(".b7_arrow_l").addClass("b7_arrow_act_l");
			$(".b7_arrow_r").addClass("b7_arrow_act_r");
		}
	});

	$("#b7_other").on("hide.bs.collapse", function (e) {
		let a = e.target.id;
		let b = this.id;
		if (a == b) {
			$(".other_progs").html("Посмотреть все программы");
			$(".b7_arrow_l").removeClass("b7_arrow_act_l");
			$(".b7_arrow_r").removeClass("b7_arrow_act_r");
		}
	});

	// меню фикс

	$(window).scroll(function () {
		if ($(window).scrollTop() > 100) {
			$("header").addClass("hdr_bg");
			$(".hdr_btn").addClass("hdr_btn_scrolled");
			$(".hdr_soc_block a").addClass("hdr_btn_scrolled");
			$(".viber_logo").addClass("viber_logo_scrolled");
		} else {
			$("header").removeClass("hdr_bg");
			$(".hdr_btn").removeClass("hdr_btn_scrolled");
			$(".hdr_soc_block a").removeClass("hdr_btn_scrolled");
			$(".viber_logo").removeClass("viber_logo_scrolled");
		}
	});

	// слайдер доп услуги

	$(".b8_slider").slick({
		infinite: true,
		slidesToShow: 4,
		slidesToScroll: 1,
		arrows: false,
		dots: true,
		responsive: [
			{
				breakpoint: 1200,
				settings: {
					slidesToShow: 3,
				},
			},
			{
				breakpoint: 992,
				settings: {
					slidesToShow: 2,
				},
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 1,
				},
			},
		],
	});

	// слайдер отзывы

	$(".b9_slider").slick({
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		dots: true,
		fade: true,
	});

	// маска телефона

	$(function () {
		$(".tel").mask("+7 (999) 999-99-99");
	});
});

// степпер калькулятора

document.addEventListener("DOMContentLoaded", () => {
	const wizard = new Zangdar("#calc_form", {
		onStepChange() {
			const breadcrumb = this.getBreadcrumb();
			buildSteps(breadcrumb);
		},
		onSubmit(e) {
			e.preventDefault();
			this.getCurrentStep().active = false;
			this.getCurrentStep().completed = true;
			const breadcrumb = this.getBreadcrumb();
			buildSteps(breadcrumb);
			e.target.style.display = "none";
			document.getElementById("form-completed").style.display = "block";
			return false;
		},
	});

	const buildBreadcrumb = () => {
		const breadcrumb = wizard.getBreadcrumb();
		buildSteps(breadcrumb);
	};

	const addStep = () => {
		const stepNumber = wizard.steps.length + 1;
		const html = `<section data-step="step-${stepNumber}">
		<h2>Step #${stepNumber}</h2>
		<div>
		  <button class="btn btn-primary float-right" data-next>Next</button>
		</div>
	  </section>`;
		wizard.getFormElement().innerHTML += html;
		wizard.refresh();
		buildBreadcrumb();
	};

	const removeStep = (index) => {
		wizard.removeStep(index);
		buildBreadcrumb();
	};

	const buildSteps = (steps) => {
		const $steps = document.getElementById("steps");

		if (document.getElementById("add__step") !== null)
			document
				.getElementById("add__step")
				.removeEventListener("click", addStep);

		if (document.getElementById("remove__step") !== null)
			document
				.getElementById("remove__step")
				.removeEventListener("click", removeStep);

		$steps.innerHTML = "";

		// let $li = document.createElement("li");
		// const $removeBtn = document.createElement("button");
		// $removeBtn.setAttribute("id", "remove__step");
		// $removeBtn.classList.add("btn", "btn-error");
		// $removeBtn.addEventListener("click", () => {
		// 	const index = wizard.steps.length - 1;
		// 	if (index < 3) return;
		// 	removeStep(index);
		// });
		// $removeBtn.innerHTML = "-";
		// $li.appendChild($removeBtn);
		// $steps.appendChild($li);

		for (let label in steps) {
			if (steps.hasOwnProperty(label)) {
				const $li = document.createElement("li");
				const $a = document.createElement("a");
				$li.classList.add("step-item");
				if (steps[label].active) {
					$li.classList.add("active");
				}
				$a.setAttribute("href", "#");
				$a.classList.add("tooltip");
				$a.dataset.tooltip = label;
				$a.innerText = label;
				$a.addEventListener("click", (e) => {
					e.preventDefault();
					wizard.revealStep(label);
				});
				$li.appendChild($a);
				$steps.appendChild($li);
			}
		}

		// $li = document.createElement("li");
		// const $addBtn = document.createElement("button");
		// $addBtn.setAttribute("id", "add__step");
		// $addBtn.classList.add("btn", "btn-primary");
		// $addBtn.addEventListener("click", addStep);
		// $addBtn.innerHTML = "+";
		// $li.appendChild($addBtn);
		// $steps.appendChild($li);
	};

	buildBreadcrumb();
});
